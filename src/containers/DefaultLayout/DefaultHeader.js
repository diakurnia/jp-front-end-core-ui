import React, { Component } from 'react';
import { Nav, Button } from 'reactstrap';

import {  AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logo from '../../assets/img/brand/logo-binar-fix.svg'


class DefaultHeader extends Component {
  render() {
    return (
      <React.Fragment >
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: 'Binar Logo' }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        <Nav className="ml-auto" navbar>
          <h6> Budi Handoko</h6>
          <Button active block color="danger" aria-pressed="true" style={{background:"#F67280", borderRadius:"4px"}}>Log Out</Button>
        </Nav>
      </React.Fragment>
    );
  }
}



export default DefaultHeader;
