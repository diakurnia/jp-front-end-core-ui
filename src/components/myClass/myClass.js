import React, { Component } from 'react';
import { Button, Card} from 'reactstrap';
import sabrina from '../../assets/img/brand/sabrina.jpeg'
import './myClass.css'

class MyClass extends Component {
  render() {
    return (
      <Card>
            <i className="cid-chart-pie"></i>
            <img class="rounded mx-auto d-block" src={sabrina} alt="gambar orang"/>
            <div >
              <p className="question-word">kamu belum punya kelas nih....</p>
              <Button className="join-button" color="warning">Gabung Bootcamp</Button>
           </div>
      </Card>
    );
  }
}

export default MyClass;
