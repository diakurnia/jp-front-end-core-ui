import React from 'react';

const MyClass = React.lazy(() => import('./components/myClass/myClass'))


const routes = [
  { path: '/', exact:true, name:'Home'},
  { path: '/kelas-saya', name: 'Kelas-saya', component: MyClass }

];

export default routes;
