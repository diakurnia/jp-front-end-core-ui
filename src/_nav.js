export default {
  items: [
    {
      name: 'Dashboard',
      url: '/dashboard',
      badge: {
        variant: 'info'
      },
    },
    {
      name: 'Kelas Saya',
      url: '/Kelas-Saya',
      icon: 'pie-lightbulb',
    },
    {
      name: 'Ambil Test',
      url: '/ambil-test',
      icon: 'icon-cursor'
    },
    {
      name: 'Bahan Belajar',
      url: '/bahan-belajar',
      icon: 'icon-cil-lightbulb'
    },
    {
      name: 'Commitment Letter',
      url: '/commitment-letter',
      icon: 'icon-bell'
    },
    {
      name: 'Playbook',
      url: '/palybook',
      icon: 'icon-ban'
    }
  ],
};
